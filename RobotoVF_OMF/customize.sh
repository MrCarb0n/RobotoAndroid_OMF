. ${SH:=$MODPATH/ohmyfont}

### INSTALLATION ###
ui_print '- Installing'
ui_print " _____     _       _       _____ ____
| __  |___| |_ ___| |_ ___|  |  |  __|
|    -| . | . | . |  _| . |  |  |  __|
|__|__|___|___|___|_| |___|\___/|_|
"

ui_print '- Unpacking font resources'
mkdir -p $MODPATH/fonts && \
tar xf $MODPATH/fonts.xz -C $MODPATH/fonts
ui_print '  Done'

ui_print '+ Prepare'
prep
ui_print '  Done'

ui_print '+ Configure'
config
ui_print '  Done'

ui_print '+ Font'
install_font
ui_print '  Done'

ui_print '+ Looking for extensions'
src
ui_print '  Done'

ui_print '+ Installing ROM patch'
rom
ui_print '  Done'

ui_print '- Finalizing'
fontspoof
svc
finish
