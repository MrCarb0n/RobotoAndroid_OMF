### RobotoAndroid_OMF
> Roboto/Flex variable font for magisk rooted android..

### Download
[RobotoVF][1] | [RobotoFlexVF][2]

[1]: https://gitlab.com/MrCarb0n/RobotoAndroid_OMF/-/raw/master/releases/RobotoVF_OMF.zip?inline=false
[2]: https://gitlab.com/MrCarb0n/RobotoAndroid_OMF/-/raw/master/releases/RobotoFlexVF_OMF.zip?inline=false
